import os
import sys

from app import app
from flask import render_template
from models import UserForm

@app.route('/')
@app.route('/index.html')
@app.route('/index.php')
def index():
    return render_template('index.html')


@app.route('/signin', methods=['GET', 'POST'])
def signin():
	form = UserForm()

	return render_template ('sigin.html', title='Leave E-mail', form=form)
