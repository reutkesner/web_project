from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired	

class UserForm(FlaskForm):
    username = StringField('Usernme', validator=[DataRequired()])
    usermail = StringField('Email', validator=[DataRequired()])
    submit = SubmitField('send')
