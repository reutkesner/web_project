from flask import Flask
from tinydb import TinyDB

app=Flask(__name__)
db = TinyDB('web_app.db')
from app.template import views
